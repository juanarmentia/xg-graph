# install.packages("gbm")
# install.packages("ggpubr")
# install.packages("pdp")
# install.packages("caret")
# install.packages("data.table")
# install.packages("devtools")
# install.packages("tidyverse")
# install.packages("usethis")
# install.packages("ggplot2")
# install.packages("ggrepel")
# install.packages("ggforce")
# install.packages("ggtext")
# install.packages("xts")
# install.packages("here")
# install.packages("extrafont")
# install.packages("ggimage")
# devtools::install_github("clauswilke/ggtext")
# devtools::install_github('jelagmil/MapxGgraphR', build_opts = c("--no-resave-data", "--no-manual"), force = TRUE)

library(gbm)
library(pdp)
library(magrittr)
library(ggpubr)
library(ggrepel)
library(ggtext)
library(tidyverse)
library(data.table)
library(usethis)
library(devtools)
library(ggplot2)
library(caret)
library(png)
library(grid)
library(xts)
library(here)
library(extrafont)
library(ggimage)
library(MapxGgraphR)

# browseVignettes("MapxGgraphR")
# xGgraph()
# OptaMAPcampofutbol2()

xG_Graph <- function(csv_path, local_width, local_height, foreign_width, foreign_height) {
  
  teams <- c("Alaves", "Nanclares", "Aurrera", "Alegria", "Altzarrate", "Lakua", "Aranbizkarra", 
             "Ipar Arriaga", "Tozuga", "Hauskaitz", "San Viator", "Mercedarias", "Ansares")
  colours <- c("#005daa", "#A01714", "#C10000", "#0074E8", "#FF0000", "#F2C80E", "#BB0000",
               "#BB0000", "#000000", "#000000", "#F2C80E", "#0D5BE2", "#18A000")
  team_colors <- setNames(as.list(colours), teams)

  directorio_modelo <- paste(system.file(package="MapxGgraphR"),"/model/xG_model.rda",sep="")
  load(directorio_modelo)
  
  shots = setDF(fread(csv_path, select = c(1:16), encoding = 'UTF-8', na.strings=c("","NA")))
  
  match_id <- shots[1,]$ID
  equipo_local <- shots[1,]$LOCAL
  equipo_visitante <- shots[1,]$VISITANTE
  jornada <- shots[1,]$JOR
  fecha <- shots[1,]$FECHA
  hora <- shots[1,]$HORA
  instalaciones <- shots[1,]$INSTALACIONES
  if(equipo_local=='Alaves'){
    tiros_locales <- shots[(shots$JUGADORA != 'Rival'), ]
    tiros_visitantes <- shots[shots$JUGADORA == 'Rival', ]
  }else{
    tiros_locales <- shots[shots$JUGADORA == 'Rival', ]
    tiros_visitantes <- shots[(shots$JUGADORA != 'Rival'), ]
  }
  
  escudo_local <- readPNG(paste("Escudos/", equipo_local, ".png", sep=""))
  escudo_visitante <- readPNG(paste("Escudos/", equipo_visitante, ".png", sep=""))
  
  for(j in 1:2){
  
    if(j==1){
      partido <- match_id
      equipo <- equipo_local
      numerotiros <- nrow(tiros_locales)
      local<-'1'
      df <- tiros_locales
    }else if(j==2){
      equipo <- equipo_visitante
      numerotiros <- nrow(tiros_visitantes)
      local<-'2'
      df <- tiros_visitantes
    }
    
    if(numerotiros>0){
      for(h in 1:numerotiros){
        x <- df[h, ]$COORD_X
        y <- df[h, ]$COORD_Y
        
        min <- df[h, ]$MINUTO
    
        distancia<-((106-x)^2+((35)-(y))^2)^0.5
        
        if(y==35){
          angulo<-round(asin(0)*57.2974694,digits=2)
        }else if(y>35){
          angulo<-round(asin((y-35)/distancia)*57.2974694,digits=2)
        }else if(y<35){
          angulo<-round(asin((35-y)/distancia)*57.2974694,digits=2)
        }
        
        parte_cuerpo<-df[h, ]$'PARTE DEL CUERPO'
        if(parte_cuerpo=='Cabeza'){
          parte_cuerpo<-c('cabeza')
        }else if(parte_cuerpo=='Pie Derecho'){
          parte_cuerpo<-c('pied')
        }else if(parte_cuerpo=='Pie Izquierdo'){
          parte_cuerpo<-c('piei')
        }else if(parte_cuerpo=='Otro'){
          parte_cuerpo<-c('otraparte')
        }
        
        situacion<-df[h, ]$'SITUACION'
        
        penal<-df[h, ]$PENALTI
        if(penal==1){
          penal<-"-1"
        }else if(penal==0){
          penal<-"0"
        }
        
        caracara<-df[h, ]$'CARA A CARA'
        if(caracara==1){
          caracara<-"-1"
        }else if(caracara==0){
          caracara<-"0"
        }
        
        gol<-df[h, ]$GOL
        if(gol==1){
          gol<-1
          jugador<-df[h, ]$JUGADORA
        }else if(gol==0){
          gol<-0
          jugador<-"0"
        }
        
        prueba<-data.frame(list(h,distancia,angulo,caracara,penal,situacion,parte_cuerpo,equipo,min,gol,jugador,local))
        names(prueba)<-c('id','distancia','angulo','one2one','penalty','situacion_juego','parte_cuerpo','equipo','min','gol','jugador','local')
        prueba$distancia <- as.numeric(as.character(prueba$distancia))
        prueba$angulo <- as.numeric(as.character(prueba$angulo))
        prueba2<-dplyr::select(prueba,distancia,angulo,one2one,penalty,situacion_juego,parte_cuerpo)
        prueba$GBM <- predict(xG_model, prueba2, na.action = na.pass, type = "prob")[,"1"]

        prueba$x<-x
        prueba$y<-y
        
        if(h==1 & j==1){
          Mapa_disparos1<-prueba
        }else if (h>1 & j==1){
          Mapa_disparos1<-rbind(Mapa_disparos1,prueba)
        }else if (h==1 & j==2){
          Mapa_disparos2<-prueba
        }else if (h>1 & j==2){
          Mapa_disparos2<-rbind(Mapa_disparos2,prueba)}
        
        if(j==1){
          Goles_xg_local<-sum(Mapa_disparos1$GBM)
          Goles_real_local<-sum(Mapa_disparos1$gol)
          Mapa_disparos1$equipo<-as.character(Mapa_disparos1$equipo)
          equipo1<-dplyr::distinct(Mapa_disparos1,equipo)
        }else{
          Goles_xg_visitante<-sum(Mapa_disparos2$GBM)
          Goles_real_visitante<-sum(Mapa_disparos2$gol)
          Mapa_disparos2$equipo<-as.character(Mapa_disparos2$equipo)
          equipo2<-dplyr::distinct(Mapa_disparos2,equipo)
        }
      }
    } 
    else {
      if(j==1){
        Goles_xg_local<-0
        Goles_real_local<-0
        Mapa_disparos1<-NULL
        equipo1<-equipo_local
      }else{
        Goles_xg_visitante<-0
        Goles_real_visitante<-0
        Mapa_disparos2<-NULL
        equipo2<-equipo_visitante
      }
    }
  }
  
  ojo <- c(team_colors[[equipo_local]], team_colors[[equipo_visitante]])
  names(ojo) <- c('1','2')
  
  h <- OptaMAPcampofutbol()
  mapa <- h
    
    # ggtitle(paste("\nMapa de disparo")) +
    # Aqui dibujamos el mapa de calor de los corners
    #stat_density2d(data=polar2,aes(x=f*106,y=g*70,fill = ..level..,alpha=..level..), geom="polygon",show.legend = FALSE) +
    #scale_fill_gradient(low="yellow", high="red",aesthetics = "fill") +
    # dejo comentado la linea siguiente para el futuro, dado que con ella dibujamos la flecha del lanzamiento de corner
    # geom_segment(data=Mapa_disparos,aes(x=x*100, y=y*100, xend = 10600, yend = 3500),arrow = arrow(length = unit(0.01, "npc")))+
    #geom_point(data = shots,aes(x = ((a)),y = ((b)/15.57)*1.31,color=Tipo_tiro,size=-Dist_Shoot,shape=Remate,stroke = 1)) +
    #Dibujamos los remates
  if(!is.null(Mapa_disparos1)) {
    mapa <- mapa + geom_point(data=Mapa_disparos1,aes(x = x*100, y=y*100,size=GBM,shape=factor(gol)),stroke = 1,color=team_colors[[equipo_local]])
  }
  if(!is.null(Mapa_disparos2)) {
    mapa <- mapa + geom_point(data=Mapa_disparos2,aes(x = 10600-x*100, y=y*100,size=GBM,shape=factor(gol)),stroke = 1,color=team_colors[[equipo_visitante]])
  }
  mapa <- mapa +
    scale_shape_manual(values=c(19,1)) +
    annotate(geom="text", x=2650, y=5500, label=format(Goles_xg_local,digits=2,nsmall=2), color=team_colors[[equipo_local]],size=10) +
    annotate(geom="text", x=2650, y=6500, label=equipo1, color=team_colors[[equipo_local]],size=8, fontface = 'bold') +
    annotate(geom="text", x=7950, y=5500, label=format(Goles_xg_visitante,digits=2,nsmall=2), color=team_colors[[equipo_visitante]],size=10) +
    annotate(geom="text", x=7950, y=6500, label=equipo2, color=team_colors[[equipo_visitante]],size=8, fontface = 'bold') +
    annotation_custom(rasterGrob(escudo_local, x = unit(0.45, "npc"), y = unit(0.85, "npc"), width = local_width, height = local_height), -Inf, Inf, -Inf, Inf) + 
    annotation_custom(rasterGrob(escudo_visitante, x = unit(0.57, "npc"), y = unit(0.85, "npc"), width = foreign_width, height = foreign_height), -Inf, Inf, -Inf, Inf)
  #metemos la leyenda abajo
  #theme(legend.position="bottom")
  
  
  # names(prueba)<-c('id','distancia','angulo','one2one','penalty','situacion_juego','parte_cuerpo','equipo','min','gol','jugador')
  
  if (!is.null(Mapa_disparos1) && !is.null(Mapa_disparos2)) {
    Mapa<-rbind(Mapa_disparos1,Mapa_disparos2)
  } else if(!is.null(Mapa_disparos1)) {
    Mapa<-Mapa_disparos1
  } else if(!is.null(Mapa_disparos2)) {
    Mapa<-Mapa_disparos2
  }

  Mapa$local<-as.character(Mapa$local)
  Mapa$jugador<-as.character(Mapa$jugador)
  saveRDS(Mapa,file=paste(partido,".rds",sep=""))
  
  t1<-data.frame(list(local=1:2))
  t2<-data.frame(list(min=1:90))
  t3<-merge(t1,t2)
  t3$local<-as.character(t3$local)
  Mapa1<-left_join(t3,Mapa)
  
  Mapa1$GBM[is.na(Mapa1$GBM)] <- 0
  
  match_rollsum <- Mapa1 %>%
    group_by(min, local) %>%
    summarize(sumxg = sum(GBM)) %>%
    ungroup() %>%
    group_by(local) %>%
    mutate(rollsum = lag(cumsum(sumxg)),
           rollsum = if_else(is.na(rollsum), 0, rollsum)) %>%
    select(local, min, rollsum, sumxg) %>%
    mutate(rollsum = case_when(
      row_number() == n() & sumxg != 0 ~ rollsum + sumxg,
      TRUE ~ rollsum
    ))
  
  match_rollsum <- match_rollsum %>%
    left_join(Mapa1 %>% filter(gol == '1') %>% select(min, gol, local, jugador),
              by = c("min", "local")) %>%
    mutate(rollsum_goal = rollsum + sumxg,
           minute_goal = min + 1,
           player_label = case_when(
             gol == '1' ~ as.character(glue::glue("{jugador}: {sumxg %>% signif(digits = 2)} xG")),
             TRUE ~ ""))
  
  
  #glimpse(match_rollsum)
  
  match_xg <- Mapa1 %>%
    group_by(local,equipo) %>%
    summarize(tot_xg = sum(GBM) %>% signif(digits = 2)) %>%
    mutate(team_label = as.character(glue::glue("{equipo}: {tot_xg} xG")))
  
  tot_match_df <- match_xg %>%
    pull(tot_xg)
  
  accum_xg_local <- match_rollsum[length(match_rollsum[[1]]) - 1, ]$rollsum
  accum_xg_visitante <- match_rollsum[length(match_rollsum[[1]]), ]$rollsum
  
  if (accum_xg_local == 0) {
    aux_accum_xg_local = 0.3
  } else {
    aux_accum_xg_local = accum_xg_local
  }
  
  if (accum_xg_visitante == 0) {
    aux_accum_xg_visitante = 0.3
  } else {
    aux_accum_xg_visitante = accum_xg_visitante
  }
  
  if(accum_xg_visitante > accum_xg_local){
    coord_escudo_visitante <- 0.90
    coord_escudo_local <- (aux_accum_xg_local * 0.95 / accum_xg_visitante)
  } else {
    
    coord_escudo_visitante <- aux_accum_xg_visitante * 0.95 / accum_xg_local
    coord_escudo_local <- 0.90
  }
  
  match_rollsumxg_plot <- match_rollsum %>%
    ggplot(aes(x = min, y = rollsum,
               group = local, color = local)) +
    geom_line(size = 2.5) +
    geom_label_repel(data = match_rollsum %>% filter(gol == '1'),
                     aes(x = minute_goal, y = rollsum_goal,
                         color = local, label = player_label),
                     nudge_x = 6, nudge_y = 0.15, family = "sans",
                     show.legend = FALSE,size=3) +
    geom_point(data = match_rollsum %>% filter(gol == '1'),
               aes(x = minute_goal, y = rollsum_goal, color = local), show.legend = FALSE,
               size = 5, shape = 21, fill = "white", stroke = 0.6) +
    scale_color_manual(values = ojo,
                       labels = c(equipo1,
                                  equipo2)) +
    scale_fill_manual(values = ojo)+
    
    scale_x_continuous(breaks = c(seq(0, 80, by = 5), 84),
                       labels = c(seq(0, 35, by = 5), "HT", 
                                  seq(45, 80, by = 5), "FT"),
                       expand = c(0.01, 0),
                       limits = c(0, 110)) +
    scale_y_continuous(sec.axis = sec_axis(~ ., breaks = tot_match_df)) +
    labs(title = paste(equipo1," (",Goles_real_local,") - ",equipo2," (",Goles_real_visitante,")",sep=''),
         subtitle = paste("Jor.  ", toString(jornada)," - ", fecha, " ", hora, " (", instalaciones, ") \nAcumulado de xG", sep=""),
         x = NULL,
         y = "Expected Goals") +
    theme_minimal() +
    theme(text = element_text(family = "sans"),
          plot.title = element_text(size = 18, family = "sans"),
          plot.subtitle = element_text(size = 14, family = "sans",
                                       color = "grey20"),
          axis.title = element_text(size = 8, color = "grey20"),
          axis.text = element_text(size = 8, face = "bold"),
          panel.grid.minor = element_blank(),
          legend.text = element_text(size = 8),
          legend.position = c(0.2, 2),
          legend.direction = "horizontal",
          legend.title = element_blank()) +
    annotation_custom(rasterGrob(escudo_local, x = unit(0.9, "npc"), y = unit(coord_escudo_local, "npc"), width = local_width, height = local_height), -Inf, Inf, -Inf, Inf) +
    annotation_custom(rasterGrob(escudo_visitante, x = unit(0.9, "npc"), y = unit(coord_escudo_visitante, "npc"), width = foreign_width, height = foreign_height), -Inf, Inf, -Inf, Inf)
  
  figure <- ggarrange(match_rollsumxg_plot,mapa,ncol = 1, nrow = 2)
  plot(figure)
  
  ggsave(paste("Images/", partido, "_Jor", jornada, ".png",sep=''),plot=figure,
         width = 9.5, height = 10, dpi = 320, units = "in", device='png')
  ggsave(paste("Images/", partido, "_Jor", jornada, "_xG.png",sep=''),plot=match_rollsumxg_plot,
         width = 9.5, height = 5, dpi = 320, units = "in", device='png')
  ggsave(paste("Images/", partido, "_Jor", jornada, "_mapa.png",sep=''),plot=mapa,
         width = 9, height = 5, dpi = 320, units = "in", device='png')
}

